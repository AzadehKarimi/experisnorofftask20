﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Task19.Model;

namespace Task19
{
    public partial class Form1 : Form
    {
        private Task19DBContext task19;
        int id;
        public Form1()
        {

            InitializeComponent();
            task19 = new Task19DBContext();
            List<Supervisor> supervisors = task19.Supervisors.ToList();
            foreach (Supervisor item in supervisors)
            {
                //add each to some component

                SupervisorCb.Items.Add(item);
            }
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            LoadStudenTable();
        }
        private void LoadStudenTable()
        {
            // this allows us to tie a DGV to a given source
            BindingSource myBindingSource = new BindingSource();
            SupervisorGV.DataSource = myBindingSource;
            myBindingSource.DataSource = task19.Students.ToList();
            //it will accept collection sources stick to list
            SupervisorGV.Refresh();
        }
        private void Button4_Click(object sender, EventArgs e)
        {
        }

        private void CreatBtn_Click(object sender, EventArgs e)
        {
            Student obj = new Model.Student
            {

                Name = NameTxt.Text,
                SupervisorId = ((Supervisor)SupervisorCb.SelectedItem).Id
            };
            task19.Students.Add(obj);
            task19.SaveChanges();
            LoadStudenTable();
        }
        private void gridClick(object sender, EventArgs e)
        {
            
        }
        //id = Convert.ToInt32(SupervisorGV.SelectedRows[0].Cells[0].Value);

       
        private void DeleteBtn_Click(object sender, EventArgs e)
        {
            if (SupervisorGV.SelectedRows.Count!=0)
            {
                id = Convert.ToInt32(SupervisorGV.SelectedRows[0].Cells[0].Value);
                // find the object in the DBSet you want to delete
                Student tempObj = task19.Students.Find(id);
                //Remove it
                task19.Students.Remove(tempObj);
                MessageBox.Show("Delete Successfull");
                task19.SaveChanges();
                LoadStudenTable();

            }


            else
                MessageBox.Show("You have to select the value from data grid view");

        }

        private void InsertBtn_Click(object sender, EventArgs e)
        {

        }
    }
}
