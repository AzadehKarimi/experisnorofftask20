﻿namespace Task19
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SupervisorGV = new System.Windows.Forms.DataGridView();
            this.DeleteBtn = new System.Windows.Forms.Button();
            this.NameTxt = new System.Windows.Forms.TextBox();
            this.SupervisorCb = new System.Windows.Forms.ComboBox();
            this.CreatBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.SupervisorGV)).BeginInit();
            this.SuspendLayout();
            // 
            // SupervisorGV
            // 
            this.SupervisorGV.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.SupervisorGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.SupervisorGV.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.SupervisorGV.Location = new System.Drawing.Point(662, 13);
            this.SupervisorGV.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.SupervisorGV.Name = "SupervisorGV";
            this.SupervisorGV.RowHeadersWidth = 51;
            this.SupervisorGV.RowTemplate.Height = 24;
            this.SupervisorGV.Size = new System.Drawing.Size(485, 466);
            this.SupervisorGV.TabIndex = 0;
            this.SupervisorGV.Click += new System.EventHandler(this.gridClick);
            // 
            // DeleteBtn
            // 
            this.DeleteBtn.Location = new System.Drawing.Point(317, 405);
            this.DeleteBtn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.DeleteBtn.Name = "DeleteBtn";
            this.DeleteBtn.Size = new System.Drawing.Size(170, 33);
            this.DeleteBtn.TabIndex = 1;
            this.DeleteBtn.Text = "Delete";
            this.DeleteBtn.UseVisualStyleBackColor = true;
            this.DeleteBtn.Click += new System.EventHandler(this.DeleteBtn_Click);
            // 
            // NameTxt
            // 
            this.NameTxt.Location = new System.Drawing.Point(214, 69);
            this.NameTxt.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.NameTxt.Name = "NameTxt";
            this.NameTxt.Size = new System.Drawing.Size(192, 30);
            this.NameTxt.TabIndex = 7;
            // 
            // SupervisorCb
            // 
            this.SupervisorCb.FormattingEnabled = true;
            this.SupervisorCb.Location = new System.Drawing.Point(204, 209);
            this.SupervisorCb.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.SupervisorCb.Name = "SupervisorCb";
            this.SupervisorCb.Size = new System.Drawing.Size(192, 31);
            this.SupervisorCb.TabIndex = 8;
            // 
            // CreatBtn
            // 
            this.CreatBtn.Location = new System.Drawing.Point(37, 403);
            this.CreatBtn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.CreatBtn.Name = "CreatBtn";
            this.CreatBtn.Size = new System.Drawing.Size(170, 35);
            this.CreatBtn.TabIndex = 9;
            this.CreatBtn.Text = "Create";
            this.CreatBtn.UseVisualStyleBackColor = true;
            this.CreatBtn.Click += new System.EventHandler(this.CreatBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 69);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(173, 23);
            this.label1.TabIndex = 10;
            this.label1.Text = "Enter Student Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 209);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(149, 23);
            this.label2.TabIndex = 11;
            this.label2.Text = "Select Supervisor";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 23F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1151, 647);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.CreatBtn);
            this.Controls.Add(this.SupervisorCb);
            this.Controls.Add(this.NameTxt);
            this.Controls.Add(this.DeleteBtn);
            this.Controls.Add(this.SupervisorGV);
            this.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "Form1";
            this.Text = "Student Form";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.SupervisorGV)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView SupervisorGV;
        private System.Windows.Forms.Button DeleteBtn;
        private System.Windows.Forms.TextBox NameTxt;
        private System.Windows.Forms.ComboBox SupervisorCb;
        private System.Windows.Forms.Button CreatBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}

